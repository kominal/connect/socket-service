import io from 'socket.io';
import { verify } from 'jsonwebtoken';
import { JWT_KEY, TASK_ID, STACK_NAME, SERVICE_TOKEN } from '@kominal/service-util/helper/environment';
import service from './index';
import axios from 'axios';

export const connections: Connection[] = [];

export class Connection {
	public interestingUserIds: string[] = [];
	public interestingGroupIds: string[] = [];

	public connectionId: string | undefined;
	public userId: string | undefined;

	constructor(public socket: io.Socket) {
		this.connectionId = socket.id;
		socket.on('REGISTER', (jwt) => this.onRegister(jwt));
		socket.on('UNREGISTER', () => this.onUnregister(true));
		socket.on('disconnect', () => this.onDisconnect());

		socket.on('PUBLISH', (packet) => this.onPublish(packet));
	}

	async onRegister(jwt: any) {
		if (jwt == null) {
			return;
		}
		const decodedToken = <any>verify(jwt, JWT_KEY);
		if (decodedToken.exp > Date.now()) {
			await this.onUnregister(false);
			this.userId = decodedToken.userId;
			connections.push(this);
			await this.refreshInterestingValues(false);
			const { connectionId, userId, interestingUserIds, interestingGroupIds } = this;
			const smqClient = service.getSMQClient();
			if (smqClient) {
				smqClient.subscribe('TOPIC', `DIRECT.CONNECTION.${connectionId}`);
				smqClient.subscribe('TOPIC', `DIRECT.USER.${userId}`);
				smqClient.subscribe('TOPIC', `INTERNAL.REFRESH.${userId}`);
				smqClient.publish('QUEUE', 'STATUS.CONNECT', 'CONNECT', { taskId: TASK_ID, connectionId, userId, interestingUserIds });
				smqClient.publish('QUEUE', 'CALL.CONNECT', 'CONNECT', { taskId: TASK_ID, connectionId, userId, interestingGroupIds });
				smqClient.publish('QUEUE', 'NOTIFICATION.CONNECT', 'CONNECT', { taskId: TASK_ID, connectionId, userId });

				console.log('After registation', smqClient.queues, smqClient.topics);
			}
		}
	}

	async onUnregister(disconnect: boolean) {
		if (connections.includes(this)) {
			connections.splice(connections.indexOf(this), 1);
		}
		if (this.userId != null) {
			const { connectionId, userId } = this;
			await this.refreshInterestingValues(disconnect);
			const smqClient = service.getSMQClient();
			if (smqClient) {
				smqClient.publish('QUEUE', 'STATUS.DISCONNECT', 'DISCONNECT', { taskId: TASK_ID, connectionId, userId });
				smqClient.publish('QUEUE', 'CALL.DISCONNECT', 'DISCONNECT', { taskId: TASK_ID, connectionId, userId });
				smqClient.publish('QUEUE', 'NOTIFICATION.DISCONNECT', 'DISCONNECT', { taskId: TASK_ID, connectionId, userId });
				smqClient.unsubscribe('TOPIC', `DIRECT.CONNECTION.${connectionId}`);
				if (!connections.find((c) => c.userId === userId)) {
					smqClient.unsubscribe('TOPIC', `INTERNAL.REFRESH.${userId}`);
					smqClient.unsubscribe('TOPIC', `DIRECT.USER.${userId}`);
				}

				console.log('After unregistation', smqClient.queues, smqClient.topics);
			}
			this.userId = undefined;
		}
	}

	async onDisconnect() {
		await this.onUnregister(true);
	}

	async refreshInterestingValues(disconnect: boolean) {
		const oldInterestingUserIds: string[] = this.interestingUserIds;
		const oldInterestingGroupIds: string[] = this.interestingGroupIds;
		this.interestingUserIds = [];
		this.interestingGroupIds = [];

		if (this.userId && !disconnect) {
			this.interestingUserIds.push(this.userId);
			const response = await axios.get(`http://${STACK_NAME}_chat-service:3000/groups`, {
				headers: {
					authorization: SERVICE_TOKEN,
					userId: this.userId,
				},
			});

			if (response != null && response.status == 200) {
				response.data.forEach((group: any) => {
					this.interestingGroupIds.push(group.id);
					if (group.type === 'DM') {
						this.interestingUserIds.push(group.partnerId);
					}
				});
			}
		}

		const smqClient = service.getSMQClient();
		if (smqClient) {
			oldInterestingGroupIds
				.filter((id) => !this.interestingGroupIds.includes(id))
				.filter((id) => !connections.find((c) => c.interestingGroupIds.includes(id)))
				.forEach((id) => smqClient.unsubscribe('TOPIC', `DIRECT.GROUP.${id}`));
			oldInterestingUserIds
				.filter((id) => !this.interestingUserIds.includes(id))
				.filter((id) => !connections.find((c) => c.interestingUserIds.includes(id)))
				.forEach((id) => smqClient.unsubscribe('TOPIC', `PUBLIC.USER.${id}`));
			this.interestingGroupIds
				.filter((id) => !oldInterestingGroupIds.includes(id))
				.forEach((id) => smqClient.subscribe('TOPIC', `DIRECT.GROUP.${id}`));
			this.interestingUserIds
				.filter((id) => !oldInterestingUserIds.includes(id))
				.forEach((id) => smqClient.subscribe('TOPIC', `PUBLIC.USER.${id}`));
		}
	}

	async publicInterestingValues() {
		await this.refreshInterestingValues(false);
		const { connectionId, userId, interestingGroupIds, interestingUserIds } = this;
		const smqClient = service.getSMQClient();
		if (smqClient && connectionId && userId) {
			smqClient.publish('QUEUE', 'STATUS.REFRESH', 'REFRESH', { taskId: TASK_ID, connectionId, userId, interestingUserIds });
			smqClient.publish('QUEUE', 'CALL.REFRESH', 'REFRESH', { taskId: TASK_ID, connectionId, userId, interestingGroupIds });
		}
	}

	async onPublish(message: { type: string; body?: any }) {
		let { type, body } = message;
		const smqClient = service.getSMQClient();
		if (type && smqClient && this.userId && this.connectionId) {
			if (!body) {
				body = {};
			}
			body.taskId = TASK_ID;
			body.connectionId = this.connectionId;
			body.userId = this.userId;
			smqClient.publish('QUEUE', `INSECURE.${type.split('.')[0]}`, type, body);
		}
	}
}
