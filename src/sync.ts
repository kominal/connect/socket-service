import { info } from '@kominal/service-util/helper/log';
import service from '.';

export async function sync() {
	info('Starting sync job...');
	const tasks = service.getSquad()?.getTasks() || [];
	try {
		if (tasks.length > 0) {
			const taskIds = tasks.map((task) => task.id);
			const smqClient = service.getSMQClient();
			if (smqClient) {
				smqClient.publish('QUEUE', 'STATUS.SYNC', 'SYNC', { taskIds });
				smqClient.publish('QUEUE', 'CALL.SYNC', 'SYNC', { taskIds });
				smqClient.publish('QUEUE', 'NOTIFICATION.SYNC', 'SYNC', { taskIds });
			}
		} else {
			info(`No tasks found!`);
		}
	} catch (e) {
		console.log(e);
	}
}
